# BIOS Update und Einstellungen

## Bios Update

Mittlerweile ist die Bios-Version **1.90** installiert.

Updates gestalten sich im `mur.at`-Projektraum ein wenig umständlich, da die `InternetFlash`-Funktionalität mit unserem Netzwerksetup offenbar nicht kompatibel ist. Man muss daher den Umweg über einen USB-Speicherstick und das `InstantFlash` Tool wählen.

Benötigt wird ein Speicherstick mit FAT32 Filesystem direkt auf der blockdevice level -- also nicht in einer untergeordnen Partition.

Zu beachten ist auch der Umstand, das beim Upgrade die BIOS-Einstellungen auf den Auslieferungszustand zurückgesetzt werden. Besonders ärgerlich ist dabei, das dadurch zur Videoausgabe immer die externe Grafikkarte genutzt wird. Es macht also in diesem Zusammenhang Sinn, den dortigen HDMI-Ausgang gleich zu Beginn am Monitor anzuschließen.

## BIOS Einstellungen

    OC Tweaker / Load Optimized CPU OC Settings = Turbo 4.7 GHz
               / DRAM Freq. = DDDR4-3200
    Advanced / Chipset Config. / Prim. Graph. Adaper = Onboard
                               / IPG Multi-Monitor = Enable
             / ACPI Conf. / PS2 Keyboard Power On = Any Key
